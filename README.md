=== World Countries ===



Contributors: sumit_motghare

Tags: world countries info plugin 



== Description ==



World Countries show info for world countries. You can place short code [Countries] at any pages and posts.



Major Features in World Prayer Time:



* All Countries.



Requirements:



Upload the World Countries plugin to your Website and Activate it, and place short code at any pages, post and

also custom pages [Countries]

and also enabled your php_openssl extension.



== Installation ==



Upload the World Countries plugin to your Website and Activate it, and place short code at any pages, post and

also custom pages [Countries]

and also enabled your php_openssl extension.



== APIs ==

Get Countries   : https://restcountries.eu/rest/v1/all/



== Screenshots ==



1. Activate World Countries Plugin



2. Add [Countries] Shortcode in your any page or Post and publish this post or page.



3. Go to view this page or post.
