<?php 

/**
* Plugin Name: World Countries
* Plugin URI:
* Description: World Countries Information
* Version: 2.0
* Author: Sumit Motghare
* Author URI:
**/


/***** No direct access to plugin PHP Files *****/
	defined( 'ABSPATH' ) or die( 'No script please!' );
	require_once( dirname( __FILE__ ) . '/admin/menu.php' );
	add_action( 'wp_ajax_my_action', 'WC_Ajax_Callback' );
	add_action('wp_ajax_nopriv_my_action', 'WC_Ajax_Callback');
	function WC_Ajax_Callback() {
		require( dirname(__FILE__ ) . '/admin/class-world-countries.php' );
		$ChangeCountry = sanitize_text_field($_POST['ChangeCountry']);
		$Object = new W_Countries();
		$Object->W_Countries_Initialization($ChangeCountry);
		wp_die();
	}

	function World_Countries_func( ){
		wp_enqueue_script( 'WC_Ajax_Callback', '/path/to/settings.js', array( 'jquery' ) );
		wp_localize_script( 'WC_Ajax_Callback', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
		?>
		<style type="text/css">
			.DropDownLoader{
					background-image: url('<?php echo plugins_url( 'images/loader.gif', __FILE__ ); ?>');
				    display: block;
				    height: 82px;
				    background-repeat: no-repeat;
				    margin-top: -41px;
			}
		</style>
		<script type="text/javascript">
			jQuery(function(){
				jQuery('.DropDownLoader').hide();
				jQuery(document).on('change', '#W_CountryDropDown', function(){
					var $ChangeCountry = jQuery(this).val();
					jQuery('.DropDownLoader').show();
					jQuery('#W_CountryDropDown').prop('disabled', true);
					jQuery.ajax({
						type : 'POST',
						url  : MyAjax.ajaxurl,
						data : {ChangeCountry : $ChangeCountry, 'action' : 'my_action'},
						success : function(response)
						{
							jQuery('.refreshTR').html(response);
							jQuery('.DropDownLoader').hide();
							jQuery('#W_CountryDropDown').prop('disabled', false);
						}

					});
				});
			});
		</script>
		<?php

		require( dirname( __FILE__ ) . '/admin/class-world-countries.php' );
		$Object = new W_Countries();
		echo '<div id="ShowPrayerTime">';
		?>
		<tr><td>Select Country : </td><td>
			<select name="W_CountryDropDown" id="W_CountryDropDown" valid="true" error="Please Select Country">
				<option value="">Please Select</option>
				<?php
				$JsonCountries = file_get_contents("https://restcountries.eu/rest/v1/all");
				$JsonCountries = json_decode($JsonCountries);
				$ArrayCountries = (array) $JsonCountries;
				sort($ArrayCountries);
				$W_CountryDropDown = (isset($_POST['W_CountryDropDown']) && !empty($_POST['W_CountryDropDown']) ? $_POST['W_CountryDropDown'] : '');
				foreach ($ArrayCountries as $Countries) {
					if($Countries->name == $W_CountryDropDown)
					{
						echo '<option value="'.$Countries->name.'" selected="selected">'.$Countries->name.'</option>';
					}else{
						echo '<option value="'.$Countries->name.'">'.$Countries->name.'</option>';
					}
				}
				?>
			</select>
		</td></tr>
		<script type="text/javascript">document.write(jQuery('#W_CountryDropDown').val());</script>
		<?php
		$Object->W_Countries_Initialization(1);
		echo '</div>';
	}
    add_shortcode( 'Countries', 'World_Countries_func' );
	
	function World_Countries_Act_activation()
	{
		if (get_option('World_Countries'))
		{
			delete_option( 'World_Countries' );
		}
	}
	register_activation_hook(   __FILE__ , 'World_Countries_Act_activation' );
?>