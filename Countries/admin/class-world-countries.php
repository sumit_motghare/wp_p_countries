<?php 


if(!class_exists('W_Countries'))
{
	class W_Countries
	{
		function W_Countries_Initialization($ChangeCountry = null)
		{
			$Country           = ($ChangeCountry == null)?'India':$ChangeCountry;
			$this->W_Countries_GenerateBlock($Country);
		}

		function W_Countries_GenerateBlock($Country)
		{
			$FetchResult  = $this->W_Countries_FetchResult($Country);
			return $this->W_Countries_RenderHtml($FetchResult);
		}

		function WPT_Prayer_getSchool($GetOption)
		{
			if(empty($GetOption['WPT_CountryDropDown']))
			{
				$Country = 'India';
			}else{
				$Country = $GetOption['WPT_CountryDropDown'];
			}
			return $Country;
		}

		function WPT_Prayer_GetOption()
		{
			$WPT_Get_Prayer_Times = get_option("WPT_Prayer_Times");
			return $WPT_Get_Prayer_Times;
		}

		function WPT_Countries()
		{
			$JsonCountries = file_get_contents("https://restcountries.eu/rest/v1/all");
			$JsonCountries = json_decode($JsonCountries);
			$ArrayCountries = (array) $JsonCountries;
			return sort($ArrayCountries);
		}

		function W_Countries_FetchResult($Country)
		{
			$Country = str_replace(' ', '%20', $Country);
			$Url = 'https://restcountries.eu/rest/v2/name/'.$Country.'?fields=topLevelDomain;alpha2Code;alpha3Code
			;callingCodes;timezones;currencies;flag';
			$JsonInfo = file_get_contents($Url);
			$JsonInfo = json_decode($JsonInfo);
			return ($Country == 'India')?$JsonInfo[1]:$JsonInfo[0];
		}

		function W_Countries_RenderHtml($FetchResult)
		{
			$Table = '<table class="refreshTR"><tr><td>Field</td><td>Value</td></tr>';
			foreach ($FetchResult as $key => $value) {
				if ( is_array($value) ) {
					if ($key == 'currencies') {
						foreach ($value as $arrValue) {
							$Table .= '<tr><td>'.ucwords($key).'</td>
							<td>'.  "Code = $arrValue->code, Name = $arrValue->name, Symbol = $arrValue->symbol" .'</td>
							</tr>';
						}
					} else {
						foreach ($value as $arrValue) {
							$Table .= '<tr><td>'.ucwords($key).'</td><td>'.$arrValue.'</td></tr>';
						}
					}
				}else if ($key == 'flag') {
					$Table .= '<tr><td>'.ucwords($key).'</td><td><img src="'.$value.'"></td></tr>';
				} else{
					$Table .= '<tr><td>'.ucwords($key).'</td><td>'.$value.'</td></tr>';
				}
			}
			$Table .= '</table>';
			echo $Table;
		}
	}
}





?>