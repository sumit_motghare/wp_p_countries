<?php 

/*
* Plugin Name : World Countries
* Plugin Author : Sumit Motghare
*/


/****** Register Main Menu ******/
add_action('admin_menu', 'WC_Plugin_Menu');

/****** Create Menu ******/
function WC_Plugin_Menu(){

	add_menu_page( 'World Countries', 'World Countries', 'manage_options', 'world_countries', 'WC_Page_Tuning');
	add_submenu_page( 'world_countries', 'Author', 'Author', 'manage_options', 'world_countries_plugin_author',  'WC_Page_Author');

}

/****** Create Menu Page ******/
function WC_Page_Tuning()
{
	require( dirname( __FILE__ ) . '/tuning.php' );
}

/****** Create Menu Page ******/
function WC_Page_Author()
{
	require( dirname( __FILE__ ) . '/author.php' );
}


?>